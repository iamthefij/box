'use strict';

exports = module.exports = {
    accesscontrol: require('./accesscontrol.js'),
    apps: require('./apps.js'),
    backups: require('./backups.js'),
    caas: require('./caas.js'),
    clients: require('./clients.js'),
    cloudron: require('./cloudron.js'),
    developer: require('./developer.js'),
    domains: require('./domains.js'),
    eventlog: require('./eventlog.js'),
    graphs: require('./graphs.js'),
    groups: require('./groups.js'),
    oauth2: require('./oauth2.js'),
    mail: require('./mail.js'),
    profile: require('./profile.js'),
    setup: require('./setup.js'),
    sysadmin: require('./sysadmin.js'),
    settings: require('./settings.js'),
    ssh: require('./ssh.js'),
    users: require('./users.js')
};
